from xml.etree.ElementTree import iterparse, parse
import os
import pandas as pd
import re
import argparse
from collections import defaultdict

s2c = {
    '''šola / osnovna šola razred / 1. letnik''': 9,
    '''šola / osnovna šola razred / 1. razred''': 0,
    '''šola / osnovna šola razred / 2. razred''': 1,
    '''šola / osnovna šola razred / 3. razred''': 2,
    '''šola / osnovna šola razred / 4. razred''': 3,
    '''šola / osnovna šola razred / 5. razred''': 4,
    '''šola / osnovna šola razred / 6. razred''': 5,
    '''šola / osnovna šola razred / 7. razred''': 6,
    '''šola / osnovna šola razred / 8. razred''': 7,
    '''šola / osnovna šola razred / 9. razred''': 8,
    '''šola / srednja šola razred / 1. letnik''': 9,
    '''šola / srednja šola razred / 2. letnik''': 10,
    '''šola / srednja šola razred / 3. letnik''': 11,
    '''šola / srednja šola razred / 4. letnik''': 12,
}

'''šola / srednja šola razred / 1.-4. letnik 19930'''

sub2c = {
    '''predmet / naravoslovje in tehnika''': 0,
    '''predmet / kemija''': 1,
    '''predmet / fizika''': 2,
    '''predmet / likovna umetnost''': 3,
    '''predmet / matematika''': 4,
    '''predmet / geografija''': 5,
    '''predmet / glasbena umetnost''': 6,
    '''predmet / slovenščina''': 7,
    '''predmet / naravoslovje''': 8,
    '''predmet / domovinska in državljanska kultura in etika''': 9,
    '''predmet / spoznavanje okolja''': 10,
    '''predmet / gospodinjstvo''': 11,
    '''predmet / družba''':12,
    '''predmet / biologija''':13,
    '''predmet / zgodovina''':14,
    '''predmet / tehnika in tehnologija''':15
}


def processLine(line):
    if line.strip() and line.strip()[-1] in ['!', ';', '.', '?']:
        #print(line)
        line = re.sub(r"^\d+\.", "", line)
        line = re.sub(r"^•", "", line)
        line = re.sub(r"^●", "", line)
        line = re.sub(r"^[abcčdefghijklm]\)", "", line)
        line = " ".join(line.split())
        if line and line[0].isupper():
            if not '……' in line and not '. . .' in line and not '�' in line and len(line.split()) > 2 and len(line.split()) < 100:
                return line
    return None


def read_ucbeniki_corpus(source, output_folder):
    sent_counter = 0
    word_counter = 0
    dd = defaultdict(list)
    dw = defaultdict(list)

    text = []
    sent = []
    namespace = '{http://www.tei-c.org/ns/1.0}'
    data_texts = []
    columns = ['subject', 'readability', 'text']

    for event, elem in iterparse(os.path.join(source)):
        if elem.tag==namespace + 'term':
            t = elem.text
            if t.startswith('šola /'):
                school = elem.text.strip()
            elif t.startswith('predmet /'):
                subject = elem.text.strip()
            elif t.startswith('razred /'):
                age = elem.text.strip()
        elif elem.tag==namespace + 'title':
            try:
                print(title)
                print(school, age)
                print()
            except:
                print('no data')
            title = elem.text
            if text:
                try:
                    c = s2c[school + " " + age]
                    subject = sub2c[subject]
                    data_texts.append([subject, c, " ".join([" ".join(sent) for sent in text])])
                    dd[c].extend([len(sent) for sent in text])
                except:
                    pass
                text = []
        else:
            if elem.tag in [namespace + 'w']:
                sent.append(elem.text)
                word_counter += 1
            elif elem.tag in [namespace + 'c']:
                sent.append(elem.text)
                word_counter += 1
            elif elem.tag in [namespace + 'pc']:
                sent.append(elem.text)
                word_counter += 1
            elif elem.tag in [namespace + 's'] and len(sent) > 0:
                sent = processLine(" ".join(sent).strip())
                if sent:
                    sent = sent.lower().split()
                    text.append(sent)
                    sent_counter += 1
                sent = []
        elem.clear()
    if text:
        try:
            c = s2c[school + " " + age]
            subject = sub2c[subject]
            data_texts.append([subject, c, " ".join([" ".join(sent) for sent in text])])
            dd[c].extend([len(sent) for sent in text])
        except:
            pass

    print('done')
    print(len(data_texts), data_texts[-1])
    print('Num words: ', word_counter)
    print('Num sents: ', sent_counter)
    print()
    print('Num words per class:')
    all = 0
    for k,v in dd.items():
        #print(k, sum(v)/len(v))
        print(k, sum(v))
        all += sum(v)
    print('All: ', all)
    print()

    df = pd.DataFrame(data_texts, columns=columns)
    df.to_csv(os.path.join(output_folder, 'ucbeniki_whole_docs_cleaned.csv'), index=False, encoding="utf8", sep="\t")
    for i in range(0,13):
        df_one_class = df[df['readability'] == i]
        print(str(i), df_one_class.shape[0])


def generate_classification_corpus(source, output_folder):
    sent_counter = 0
    word_counter = 0
    dd = defaultdict(int)
    ddsub = defaultdict(int)

    text = []
    sent = []
    namespace = '{http://www.tei-c.org/ns/1.0}'
    data_texts = []
    columns = ['subject', 'readability', 'text']

    for event, elem in iterparse(os.path.join(source)):
        if elem.tag==namespace + 'term':
            t = elem.text
            if t.startswith('šola /'):
                school = elem.text.strip()
            elif t.startswith('predmet /'):
                subject = elem.text.strip()
            elif t.startswith('razred /'):
                age = elem.text.strip()
        elif elem.tag==namespace + 'title':
            if text:
                try:
                    c = s2c[school + " " + age]
                    subject = sub2c[subject]
                    data_texts.append([subject, c, [" ".join(sent) for sent in text]])
                    dd[c] += len(text)
                except Exception as e:
                    print(e)
                text = []
        else:
            if elem.tag in [namespace + 'w']:
                sent.append(elem.text)
                word_counter += 1
            elif elem.tag in [namespace + 'c']:
                sent.append(elem.text)
                word_counter += 1
            elif elem.tag in [namespace + 'pc']:
                sent.append(elem.text)
                word_counter += 1
            elif elem.tag in [namespace + 's'] and len(sent) > 0:
                sent_counter += 1
                sent = processLine(" ".join(sent).strip())
                if sent:
                    sent = sent.lower().split()
                    text.append(sent)
                sent_counter += 1
                sent = []
        elem.clear()
    if text:
        try:
            c = s2c[school + " " + age]
            subject = sub2c[subject]
            data_texts.append([subject, c, [" ".join(sent) for sent in text]])
            dd[c] += len(text)
        except:
            pass

    print('done')
    print(len(data_texts), data_texts[-1])
    print('Num words: ', word_counter)
    print('Num sents: ', sent_counter)

    data_chunks = []
    for t in data_texts:
        subject = t[0]
        c = t[1]
        sents = t[2]
        for i in range(0, len(sents), 25):
            chunk = sents[i:i + 25]
            if len(chunk) > 10:
                data_chunks.append([subject, c, " ".join(chunk)])




    df = pd.DataFrame(data_chunks, columns=columns)
    print(df.shape)
    for i in range(0,13):
        df_one_class = df[df['readability'] == i]
        print(str(i), df_one_class.shape[0])
    df.to_csv(os.path.join(output_folder, 'ucbeniki_classification_docs_25_cleaned.csv'), index=False, encoding="utf8", sep="\t")

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Script for extracting the Ucbeniki corpus")
    parser.add_argument("--input_path", type=str, default="data/Ucbeniki_oznaceni/ucbeniki-sl.xml")
    parser.add_argument("--output_folder", type=str, default="data/Ucbeniki_oznaceni/")
    args = parser.parse_args()

    read_ucbeniki_corpus(args.input_path, args.output_folder)
    generate_classification_corpus(args.input_path, args.output_folder)









