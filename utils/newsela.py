import pandas as pd
from collections import defaultdict
from os import listdir
from os.path import isfile, join
import re
import nltk
import argparse


def getmetadata(input, lang='en'):
    d = {}
    s = {}
    count_classes = defaultdict(int)
    count_simpl = defaultdict(int)
    count_names = defaultdict(int)
    df = pd.read_csv(input, sep=',', encoding='utf8')
    print("All articles: ", df.shape[0])
    print(df.columns)
    en_articles_counter = 0
    for idx, row in df.iterrows():
        if row['language'] == lang:
            en_articles_counter += 1
            #if count_classes[row['grade_level']] < 224:
            d[row['filename']] = int(row['grade_level'])
            if int(row['filename'][-5]) == 5:
                s[row['filename']] = 4
            else:
                s[row['filename']] = int(row['filename'][-5])
            count_classes[row['grade_level']] += 1
            count_simpl[row['filename'][-5:]] += 1
            count_names[row['filename'][:-5]] += 1


    print("Num. eng. articles: ", en_articles_counter)
    print("Class distribution: ", count_classes)
    print("Simpl distribution: ", count_simpl)
    print("Num diff. articles: ", sorted(count_names.items(), key=lambda x: x[1]))
    print("Num documents: ", sum(count_classes.values()))
    return d,s

def create_newsela_corpus(input, output, d):
    corpus = []
    count_words = defaultdict(int)
    for f in listdir(input):
        path = join(input, f)
        if path.endswith('txt') and f in d:
            with open(path, 'r', encoding='utf8') as fajl:
                text = " ".join(fajl.read().split())
                text = re.sub(r"<img[^>]*>", '', text)
                regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
                text = re.sub(regex, '', text)
                length = len(text.split())
                count_words[d[f]] += length
                corpus.append([text, d[f]])

    all = 0
    for k, v in count_words.items():
        print(k, v)
        all += v
    print(all)

    df = pd.DataFrame(corpus, columns=['text', 'readability'])
    df.to_csv(output, sep='\t', encoding='utf8', index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Script for extracting the Newsela corpus")
    parser.add_argument("--path_to_metadata", type=str, default='data/newsela_article_corpus_2016-01-29/articles_metadata.csv')
    parser.add_argument("--path_to_articles", type=str, default='data/newsela_article_corpus_2016-01-29/articles')
    parser.add_argument("--output_file", type=str, default='data/newsela/newsela.csv')
    args = parser.parse_args()

    d,s = getmetadata(args.path_to_metadata)
    create_newsela_corpus(args.path_to_articles, args.output_file, d)





