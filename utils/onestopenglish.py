from collections import defaultdict
import pandas as pd
import argparse

def build_text_csv(input, output):
    data = []
    d = defaultdict(list)
    countclass = defaultdict(int)
    with open(input, 'r', encoding="utf8") as f:
        for line in f:
            text, label, doc = line.split('\t')
            d[doc + '|||' + label].append((text))
    for doc, text in d.items():
        label = doc.split('|||')[1]
        countclass[label] += 1
        data.append([" ".join(text), label])
    df = pd.DataFrame(data, columns=['text', 'readability'])
    print(df.shape)
    print(countclass)
    df.to_csv(output, encoding='utf8', index=False, sep="\t")


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Script for extracting the OneStopEnglish corpus")
    parser.add_argument("--input_path", type=str, default="data/onestopenglish/onestopenglish.txt")
    parser.add_argument("--output_path", type=str, default="data/onestopenglish/onestopenglish_docs.csv")
    args = parser.parse_args()
    build_text_csv(args.input_data, args.output_data)



