# Code for paper 'Supervised and unsupervised neural approaches to text readability' #


## Installation, documentation ##

Published results were produced in Python 3 programming environment on Linux Mint 18 Cinnamon operating system. Instructions for installation assume the usage of PyPI package manager.<br/>

Install dependencies if needed: pip install -r requirements.txt

### Instructions: ###

**Due to the proprietary issues, we can not share the readability datasets used in the paper. 
We do however provide scripts that convert original corpora to the csv files suitable for further processing.<br/>**


Make WeeBit csv file:<br/>
```
python /utils/extract_weebit.py --input_path pathToWeeBitFolder --output_path pathToOutputCSVFile
```

Make OneStopEnglish csv file:<br/>
```
python /utils/onestopenglish.py --input_path pathToOneStopEnglishTextFile --output_path pathToOutputCSVFile
```

Make Newsela csv file:<br/>
```
python /utils/newsela.py --path_to_metadata pathToNewselaMetadataFile --path_to_articles pathToNewselaArticlesFolder --output_path pathToOutputCSVFile
```

Make two Slovenian SB csv files, one containing entire school books and the other containing chunks of 25 sentences:<br/>
```
python /utils/sloveniansb.py --input_path pathToSlovenianSBXMLfile --output_path pathToOutputFolderThatWillContain2CSVFiles
```

**In order to reproduce results of the supervised experiments, we provide three scripts for training and evaluation of the classifiers:<br/>**


Fine-tune and evaluate BERT classifier:<br/>
```
python supervised/BERT/run_classifier.py --bert_model modelName --task_name taskName --input_path pathToCSVFileWithDataset --output_dir pathToDirWheretheModelIsSaved
```

Train and evaluate HAN classifier:<br/>
```
python supervised/HAN/train_test.py  --task_name taskName --input_path pathToCSVFileWithDataset --saved_path pathToDirWheretheModelIsSaved --vocab_path pathToOutputVocabPath 
```

Train and evaluate pooled BiLSTM classifier:<br/>
```
python supervised/pooled_BiLSTM/train_test.py  --task_name taskName --input_path pathToCSVFileWithDataset --saved_path pathToDirWheretheModelIsSaved
```

The name of the taskName should be 'onestopenglish', 'weebit', 'newsela' or 'ucbeniki' for all three classifiers, for BERT, modelName should be 'bert-base-uncased' for English datasets and 'EMBEDDIA/crosloengual-bert' for Slovenian.<br/>

**In order to reproduce results of the unsupervised experiments, we provide scripts for training the RNN and TCN language models and scripts for application of the language models to the readability datasets.
We also provide Wikipedia/Simple Wikipedia datasets for language model training:<br/>**

Train RNN language model:<br/>

```
python unsupervised/LSTMCNN/train.py  --data_dir PathToTrainingDatasetDir --checkpoint_dir PathToDirWhereCheckpointsAreSaved --savefile nameOfTheModelToSave

```

Apply RNN language model:<br/>

```
python unsupervised/LSTMCNN/apply.py --data PathToReadabilityDatasetCSVFile --output PathToFileWithResults --model PathToSavedModelFile --settings PathToSavedSettingsFile --vocabulary PathToSavedVocabFile

```

Evaluate RNN language model:<br/>

```
python unsupervised/LSTMCNN/evaluate.py  --input PathToFileWithResults

```

Train TCN language model:<br/>

```
python unsupervised/TCN/train.py  --data PathToTrainingDatasetDir --model_path nameOfTheModelToSave

```

Apply TCN language model:<br/>

```
python unsupervised/TCN/apply.py --data PathToReadabilityDatasetCSVFile  --lm_model_path PathToSavedModelFile --lm_data PathToTrainingDatasetDir  --results_path PathToFileWithResults

```

Evaluate TCN language model:<br/>

```
python unsupervised/TCN/evaluate.py  --input PathToFileWithResults

```

PathToTrainingDatasetDir should be a directory that contains train.txt and valid.txt files. 
See provided Simple Wikipedia/Wikipedia datasets for exact format of the language model train and validation sets. 


Apply BERT language model:<br/>

```
python unsupervised/BERT/apply.py --input PathToReadabilityDatasetCSVFile  --output PathToFileWithResults --lang Language

```

Evaluate TCN language model:<br/>

```
python unsupervised/TCN/evaluate.py  --input PathToFileWithResults

```

**If something is unclear, please check the default arguments for each parameter or feel free to contact us.**


* [Knowledge Technologies Department](http://kt.ijs.si), Jožef Stefan Institute, Ljubljana
