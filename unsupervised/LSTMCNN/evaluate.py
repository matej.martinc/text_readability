import pandas as pd
import argparse
from scipy.stats.stats import pearsonr
import os

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Evaluate perplexity and RSRS scores of the BERT model')
    argparser.add_argument('--input', dest='input', type=str,
                           default='unsupervised/LSTMCNN/results/simple_weebit.csv',
                           help='Choose input csv file produced by the apply.py language model script')
    args = argparser.parse_args()

    data = pd.read_csv(args.input, encoding="utf-8", delimiter="\t")
    print("Pearson perplexity results: ", pearsonr(data['class'], data['perplexity']))
    print("Pearson RSRS results: ", pearsonr(data['class'], data['score']))
    print('-------------------------------------\n')


