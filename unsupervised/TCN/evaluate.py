import pandas as pd
import argparse
from scipy.stats.stats import pearsonr
import os

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Evaluate perplexity and RSRS scores of the BERT model')
    argparser.add_argument('--input', dest='input', type=str,
                           default='unsupervised/TCN/results/balanced_onestopenglish.csv',
                           help='Choose input csv file produced by the apply.py language model script')
    args = argparser.parse_args()

    files = os.listdir('results')
    for f in files:
        input = os.path.join('results', f)
        data = pd.read_csv(input, encoding="utf-8", delimiter="\t")
        print(f, data.shape)
        print("Pearson perplexity results: ", pearsonr(data['class'], data['perplexity']))
        print("Pearson RSRS results: ", pearsonr(data['class'], data['score']))
        print('-------------------------------------\n')


